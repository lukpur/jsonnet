var app = angular.module('jsnt', []);

// Controllers
app.controller('MainController', ['getPropertyByPath', '$http', '$scope', '$timeout', 'getPropertyClassification', 'constructPath',
  function (getPropertyByPath, $http, $scope, $timeout, getPropertyClassification, constructpath) {
    $scope.errors = {loadError:{}};
    var setData = function(obj) {
      $scope.dataRoot = obj;
      $scope.viewRoot = $scope.dataRoot;
      $scope.viewPath = 'dataRoot';
      $timeout(function() {
        $(document.body).animate({
          'scrollTop':   $('#formTop').offset().top
        }, 500);
      }, 0);
    },
        ctrlDown = false;

    $scope.viewPath = 'dataRoot';

    $scope.getPropertyClassification = getPropertyClassification;

    $('#jsonData').keydown(function(evt) {
      // prevent changes in the data view
//        evt.currentTarget.value = $scope.prettyDataRoot();
      if (evt.which == 91 || evt.which == 17) {
        ctrlDown = true;
      } else if (!(ctrlDown && evt.which == 67)) {
        evt.preventDefault();
      }
    }).keyup(function(evt) {
      if (evt.which == 91 || evt.which == 17) {
        ctrlDown = false;
      }
    });

    // Custom controller functions
    $scope.dataSelectAll = function() {
//      $('#jsonData')[0].select();
      $('#jsonData')[0].setSelectionRange(0, $('#jsonData').val().length);
    };

    $scope.callLoad = function(typeFunc) {
      $scope.errors.loadError = {} // clear any load errors
      setData(undefined);
      $scope[typeFunc]();
    };
    $scope.loadJson = function() {
      var obj;
      try {
        obj = angular.fromJson($scope.dataSource.jsonSrc);
        setData(obj);
      } catch (e) {
        $scope.errors.loadError.jsonLoadError = e.message;
      }
    };

    $scope.loadRest = function() {
      $http.get($scope.dataSource.src).success(function (data) {
        setData(data);
      }).error(function(err){
            $scope.errors.loadError.restLoadError = err ? err : 'Unknown Error';
          });
    };

    $scope.prettyDataRoot = function() {
      var ret = angular.toJson($scope.dataRoot, true);
      return ret;
    };
    $scope.removeFromArray = function (arr, index) {
      arr.splice(index, 1);
    };

    $scope.pushArray = function(arr) {
      arr.push("");
    };

    $scope.isNonComplexArray = function (value) {
      var propClass = getPropertyClassification(value);
      return propClass.indexOf('array') > -1 && propClass != 'arrayComplex';
    };

    $scope.updatePath = function (newPath) {
      $scope.viewPath = newPath;
      $scope.viewRoot = getPropertyByPath(newPath, $scope.dataRoot);
      // scroll to top of form
      $(document.body).animate({
        'scrollTop':   $('#formTop').offset().top
      }, 500);
    };

    $scope.stepBack = function () {
      if ($scope.viewPath.indexOf('[') > -1) {
        $scope.updatePath($scope.viewPath.substring(0, $scope.viewPath.lastIndexOf("[")));
      }
    };

    $scope.getNameFromPath = function () {
      return $scope.viewPath.indexOf('[') < 0 ? '/' :
          $scope.viewPath.substring($scope.viewPath.lastIndexOf('[') + 2, $scope.viewPath.length - 2);
    };

    $scope.goHome = function () {
      $scope.updatePath('dataRoot');
    };

    $scope.prettyPath = function (str) {
      return str.replace(/dataRoot/, '/ ').replace(/'\]/g, " / ").replace(/\['/g, '');
    };

    $scope.constructPath = constructpath;

    //test JSON
//    $scope.dataSource = {src:'/sample/sample2.json'};
//    $scope.loadRest();
  }]);

// Services
app.factory('getPropertyClassification', [function () {
  function isPrimitive(prop) {
    return (typeof prop == 'string' ||
        typeof prop == 'number' ||
        typeof prop == 'boolean' ||
        typeof prop == 'undefined' ||
        prop == null);
  }

  function isSimpleArray(arr) {
    if (!Array.isArray(arr)) {
      return false;
    }
    for (var i = 0; i < arr.length; i += 1) {
      if (!isPrimitive(arr[i])) {
        return false;
      }
    }
    return true;
  }

  function isPrimitiveObjectArray(arr) {
    if (!Array.isArray(arr) || isSimpleArray(arr)) {
      return false;
    }
    for (var i = 0; i < arr.length; i += 1) {
      if (!(isPrimitive(arr[i]) || isPrimitiveObjectNoArray(arr[i]))) {
        return false;
      }
    }
    return true;
  }

  function isComplexArray(arr) {
    if (!Array.isArray(arr)) {
      return false;
    }
    for (var i = 0; i < arr.length; i += 1) {
      if (!isPrimitive(arr[i]) && !isPrimitiveObjectNoArray(arr[i])) {
        return true;
      }
    }
    return false;
  }

  function isPrimitiveObject(prop) {
    if (typeof prop != 'object' || Array.isArray(prop)) {
      return false;
    }
    for (var property in prop) {
      if (prop.hasOwnProperty(property)) {
        if (!isPrimitive(prop[property])) {
          if (!isSimpleArray(prop[property])) {
            return false;
          }
        }
      }
    }
    return true;
  }

  function isPrimitiveObjectNoArray(prop) {
    if (typeof prop != 'object' || Array.isArray(prop)) {
      return false;
    }
    for (var property in prop) {
      if (prop.hasOwnProperty(property)) {
        if (!isPrimitive(prop[property])) {
          return false;
        }
      }
    }
    return true;
  }

  function isComplexObject(prop) {
    if (typeof prop != 'object' || Array.isArray(prop)) {
      return false;
    }
    return !isPrimitiveObject(prop);
  }

  function isEmptyObject(prop) {
    if (typeof prop != 'object' || Array.isArray(prop)) {
      return false;
    }
    for (var property in prop) {
      return false;
    }
    return true;
  }

  function classify(prop) {
    if (isPrimitive(prop)) {
      return 'primitive';
    } else if (isSimpleArray(prop)) {
      return prop.length == 0 ? 'arrayEmpty' : 'arraySimple';
    } else if (isPrimitiveObjectArray(prop)) {
      return 'arrayPrimitiveObject';
    } else if (isComplexArray(prop)) {
      return 'arrayComplex';
    } else if (isPrimitiveObject(prop)) {
      return isEmptyObject(prop) ? 'objectEmpty' :
          (isPrimitiveObjectNoArray(prop) ? 'objectPrimitiveNoArray' : 'objectPrimitive');
    } else if (isComplexObject(prop)) {
      return 'objectComplex';
    } else {
      return 'unclassified';
    }
  }

  return classify;
}]);

app.factory('constructPath', [function () {
  return function (root, node) {
    return root + '[\'' + node + '\']';
  };
}]);

app.factory('getPropertyByPath', [function () {
  return function (path, obj) {
//        var reg = /\[\'([^'])*\'\]/g;
    var reg = /\[\'(.+?)\'\]/g;
    var matches = path.match(reg);
    var objToReturn = obj;
    var i = 0;
    var curProp;
    if (!matches) {
      return obj; //not a valid path, therefore assume root
    }
    for (; i < matches.length; i += 1) {
      curProp = reg.exec(matches[i])[1];
      reg.lastIndex = 0;
      objToReturn = objToReturn[curProp];
    }
    return objToReturn;
  }
}]);

// Directives
app.directive('primitiveField', [function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/primitiveFieldInBSHorizontalForm',
    scope: {
      path: '@',
      fieldLabel: '@',
      boundObj: '='
    },
    replace: true
  }
}]);

app.directive('primitiveObject', ['getPropertyClassification', 'constructPath', function(getPropertyClassification, constructPath) {
  return {
    restrict: 'E',
    templateUrl: 'templates/primitiveObjectInBSHorizontalForm',
    scope: {
      path: '@',
      fieldLabel: '@',
      boundObj: '='
    },
    replace: true,
    link: function(scope, element, attrs) {
      scope.getPropertyClassification = getPropertyClassification;
      scope.constructPath = constructPath;
    }
  }
}]);

app.directive('array', ['getPropertyClassification', 'constructPath', function(getPropertyClassification, constructPath) {
  return {
    restrict:'E',
    templateUrl: 'templates/arrayInBSHorizontalForm',
    scope: {
      path: '@',
      fieldLabel: '@',
      boundObj: '=',
      pathFunc: '&'
    },
    replace: true,
    link: function(scope, element, attrs) {
      scope.getPropertyClassification = getPropertyClassification;
      scope.constructPath = constructPath;
      scope.addItemToArray = function() {
        scope.boundObj.push("");
      }
    }
  }
}]);

app.directive('primitiveArrayItem', [function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/primitiveItemInArray',
    scope: {
      path: '@',
      fieldLabel: '@',
      boundObj: '='
    },
    replace: true,
    controller: function($scope) {
      $scope.removeFromArray = function(arr, index) {
        arr.splice(index, 1);
      }
    }
  }
}]);

app.directive('primitiveObjectArrayItem', ['getPropertyClassification', function(getPropertyClassification) {
  return {
    restrict: 'E',
    templateUrl: 'templates/primitiveObjectItemInArray',
    scope: {
      path: '@',
      fieldLabel: '@',
      boundObj: '='
    },
    replace: true,
    controller: function($scope) {
      function getFirstValue() {
        var ret = undefined;
        angular.forEach($scope.boundObj[$scope.fieldLabel], function(val, key) {
          if (val && typeof val != 'object') {
            ret = ret || key + ": " + val;
          }
        });
        return ret;
      }
      var firstVal = getFirstValue();
      $scope.label = firstVal || 'Multiple fields';
      $scope.isVisible = false;
      $scope.getPropertyClassification = getPropertyClassification;
      $scope.removeFromArray = function(arr, index) {
        arr.splice(index, 1);
      }
      $scope.toggleVisible = function() {
        $scope.isVisible = !$scope.isVisible;
      };
    }
  }
}]);

app.directive('complexObjectArrayItem', [function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/complexObjectItemInArray',
    scope: {
      path: '@',
      fieldLabel: '@',
      boundObj: '=',
      pathFunc: '&'
    },
    replace: true,
    controller: function($scope) {
      function getFirstValue() {
        var ret = undefined;
        angular.forEach($scope.boundObj[$scope.fieldLabel], function(val, key) {
          if (val && typeof val != 'object') {
            ret = ret || key + ": " + val;
          }
        });
        return ret;
      }
      $scope.removeFromArray = function(arr, index) {
        arr.splice(index, 1);
      }
      var firstVal = getFirstValue();
      $scope.label = firstVal || "Complex Object";

    }
  }
}]);

app.directive('complexObject', [function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/complexFieldInBSHorizontalForm',
    scope: {
      path: '@',
      fieldLabel: '@',
      pathFunc: '&'
    },
    replace: true
  }
}]);

app.directive('jsntGraph', [function() {
  return {
    restrict: 'E',
    template: '<svg id="graph"><g id="svgGroup"></g></svg>',
    replace: true,
    scope: {
      dataRoot: '=',
      viewRoot: '='
    },
    controller: function($scope) {
      var scope = $scope;
      $scope.$watch('viewRoot', function(newData) {
        function createDataTree(srcData) {
          function processValue(parent, data) {
            if(typeof data == 'object') {
              parent.children = [];
              angular.forEach(data, function(value, key) {
                var item = {name: key};
                processValue(item, value);
                parent.children.push(item);
              });
            } else {
              parent.value = data;
            }
          }
          var retTree = {},
              dataCopy = angular.copy(srcData);
          if (dataCopy) {
            retTree.name = "/";
            processValue(retTree, dataCopy);
          }
          return retTree;
        }
        var size = {
          width: 500,
          height: 500,
          margin: 20
        };
        var dataTree = createDataTree(newData);
        if (dataTree){
          var tree = d3.layout.tree()
              .size([size.width-size.margin*2, size.height - size.margin*2]);
          var nodes = tree.nodes(dataTree);
          var links = tree.links(nodes);

          var graph = d3.select('#graph')
              .attr('width', size.width)
              .attr('height', size.height);

          var group = d3.select('#svgGroup');
          group.attr('transform', 'translate(' + size.margin + ',' + size.margin + ')');

          var link = d3.svg.diagonal();

          var linkPaths = group.selectAll("path.link")
              .data(links);
          linkPaths.enter()
              .append('svg:path')
              .attr('class', 'link')
              .attr('d', link);
          linkPaths.exit()
              .remove();

          linkPaths.transition().duration(500)
              .attr('d', link);

          var nodeGroup = group.selectAll('g.node')
              .data(nodes);
          var nodeGroupEnter = nodeGroup.enter().append('svg:g')
              .attr('class', 'node')
          nodeGroupEnter.append('svg:circle')
              .attr('class', 'node-dot')
              .attr('r', '5');
          nodeGroupEnter.append('svg:text')
              .attr('dy', -15)
              .text(function(d) {
                return typeof d.name == 'number' ? d.value: d.name;
              })
              .attr('text-anchor', 'middle');

          var nodeUpdate = nodeGroup.transition().duration(500)
                .attr('transform', function(d) {
                  return 'translate(' + d.x + ',' + d.y + ')';
                });

          nodeUpdate.select('text')
              .text(function(d){
                return typeof d.name == 'number' ? d.value: d.name;
              })
          nodeGroup.exit()
              .remove();
        }
      }, true);
    }
  }
}]);
