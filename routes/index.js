/*
 * GET home page.
 */

exports.index = function (req, res) {
  res.render('index', { title: 'Jsonnet' });
};

exports.getTemplate = function (req, res) {
    res.render('templates/' + req.params.tmpl, {}, function(err, html) {
      if (err) {
        res.render('templateNotFound');
      } else {
        res.end(html);
      }
    });
}

exports.renderJade = function (req, res) {
  res.render(''+ req.params.jadeFile, {}, function(err, html) {
    if (err) {
      res.render('jadeFileNotFound');
    } else {
      res.end(html);
    }
  });
}
